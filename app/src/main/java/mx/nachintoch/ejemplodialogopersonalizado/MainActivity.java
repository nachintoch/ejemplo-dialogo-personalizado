package mx.nachintoch.ejemplodialogopersonalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    DialogoPersonalizado dialogoPersonalizado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Paso 1) Crear recursos - Actividad
        // Paso 2) Definir Actividad
        // Paso 3) Crear recursos - Dialogo
        // Paso 4) Definir el dialogo
        // Paso 5) Invocar aquí
        Button button = findViewById(R.id.mostrar_dialogo_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDialogo();
            }
        });
        mostrarDialogo();
    }

    private void mostrarDialogo() {
        if (dialogoPersonalizado == null) {
            dialogoPersonalizado = new DialogoPersonalizado();
        }
        dialogoPersonalizado.show(getSupportFragmentManager(), "FOSS");
    }


}